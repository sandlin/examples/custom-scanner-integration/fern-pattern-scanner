package main

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/scanner"
)

var version = "0.0.5"

func main() {
	app := &cli.App{
		Name:     "fern-pattern-scanner",
		Usage:    "scans for patterns in files based off of a configured rule set",
		Compiled: time.Now(),
		Authors: []*cli.Author{
			&cli.Author{
				Name:  "Fernando Diaz",
				Email: "fdiaz@gitlab.com",
			},
		},
		Copyright: "(c) 2023 GitLab",
		Version:   version,
		Commands: []*cli.Command{
			{
				Name:  "scan",
				Usage: "scan a directory for patterns",
				Action: func(cCtx *cli.Context) error {

					// Set the location of the rules from flag
					rulesPath := cCtx.String("rules")

					// Load the rules file from the provided path
					rulesFile, err := os.ReadFile(rulesPath)
					if err != nil {
						log.Fatal(err)
					}

					// Set the location of the path to scan from flag
					scanDir := cCtx.String("path")
					if scanDir == "" {
						// Load current directory to scan
						scanDir, err = filepath.Abs("")
						if err != nil {
							log.Fatal(err)
						}
					}

					// Run the scanner
					results, start, end := runScanner(rulesFile, scanDir)

					// Generate a report if flag is set
					if cCtx.Bool("report") {
						runReport(results, start, end)
					}

					return nil
				},
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "rules",
						Value:    "",
						Usage:    "loads a rules file with patterns and files to scan",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "path",
						Value:    "",
						Usage:    "set the path to scan",
						Required: false,
					},
					&cli.BoolFlag{
						Name:     "report",
						Value:    false,
						Usage:    "generates a secret detection report for GitLab CI",
						Required: false,
					},
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

// Runs the scanner
func runScanner(rules []byte, path string) ([]scanner.Vulnerability, time.Time, time.Time) {
	// Get the start time
	start := time.Now()

	// Run the scanner
	results := scanner.Scan(rules, path)

	// Get the end time
	end := time.Now()

	return results, start, end
}

// Creates a report
func runReport(results []scanner.Vulnerability, start time.Time, end time.Time) {
	// Get the commit info from GitLab if applicable
	commit := scanner.Commit{
		Author:  os.Getenv("CI_COMMIT_AUTHOR"),
		Date:    os.Getenv("CI_JOB_STARTED_AT"),
		Message: strings.ReplaceAll(os.Getenv("CI_COMMIT_MESSAGE"), "\n", ""),
		SHA:     os.Getenv("CI_COMMIT_SHA"),
	}

	// Generate a report struct to parse
	RFC3339UTC := "2006-01-02T15:04:05"
	report := scanner.Report{
		Start:           start.Format(RFC3339UTC),
		End:             end.Format(RFC3339UTC),
		Vulnerabilities: results,
		Commit:          commit,
		Version:         version,
		Status:          "success",
	}

	// Generate a report using templates
	scanner.GenerateReport(report)
}
