package scanner

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gopkg.in/yaml.v2"
)

// Scans directory for files
func detectFiles(scanDir string) ([]string, error) {
	files := []string{}

	// Parse the excluded paths and remove empty strings
	skipDirs := strings.Split(os.Getenv("FERN_ANALYZER_EXCLUDED_PATHS"), ",")
	skipDirs = removeEmptyStrings(skipDirs)

	// Append the CI_PROJECT_DIR when running on GitLab
	gitlabProjectDir := os.Getenv("CI_PROJECT_DIR")
	if gitlabProjectDir != "" {
		// Add .git to the excluded paths
		skipDirs = append(skipDirs, ".git")

		gitlabProjectDir = gitlabProjectDir + "/"

		gitlabSkipDirs := []string{}
		for _, dir := range skipDirs {
			gitlabSkipDirs = append(gitlabSkipDirs, gitlabProjectDir+dir)
		}

		skipDirs = gitlabSkipDirs
	}

	log.Info(fmt.Sprintf("Skipping the following directories: %+v", skipDirs))

	// Scan all files in directory and subdirectories
	err := filepath.Walk(scanDir,
		func(path string, info os.FileInfo, err error) error {
			// TODO: Add debug logging
			// If it's a excluded directory/file, skip it
			skip := false
			if !info.IsDir() {
				for _, skipDir := range skipDirs {
					if strings.Contains(path+info.Name(), skipDir) {
						skip = true
						break
					}
				}

				if !skip {
					files = append(files, path)
				}
			}
			return nil
		})

	return files, err
}

// Removes empty strings from list
func removeEmptyStrings(original []string) []string {
	new := []string{}
	for _, item := range original {
		if item != "" {
			new = append(new, item)
		}
	}
	return new
}

// Loads the regex rules for detecting patterns
func loadRules(file []byte) (Rules, error) {
	// Unmarshal the rules into a struct
	rules := Rules{}
	err := yaml.Unmarshal(file, &rules)
	if err != nil {
		log.Fatal(err)
	}

	return rules, nil
}

// Scans json file and writes a file with detected patterns
func Scan(rulesFile []byte, scanDir string) []Vulnerability {
	log.Info("The scanner is starting")

	// Stores all the scanner results to later generate a report
	scanner_results := []Vulnerability{}

	// Loads Regex rules for pattern matching
	rules, err := loadRules(rulesFile)
	if err != nil {
		log.Fatal(err)
	}

	// Loads files in selected directory
	files, err := detectFiles(scanDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		// Open the file
		result, err := os.Open(file)
		if err != nil {
			log.Fatal(err)
		}
		defer result.Close()

		// Create a scanner and keep track of it's position
		scanner := bufio.NewScanner(result)
		scannerPos := 0

		// Scan through a file line by line
		for scanner.Scan() {
			scannerPos = scannerPos + 1

			// TODO: Run rule comparisons in parallel
			for _, rule := range rules.Patterns {
				// Check of the file contains the specified rule
				match, err := regexp.MatchString(rule.Pattern, scanner.Text())
				if err != nil {
					log.Fatal(err)
				}
				// If there is a match, then create a vulnerability and add it to list
				if match {
					// Remove the CI_PROJECT_DIR when running on GitLab
					gitlabProjectDir := os.Getenv("CI_PROJECT_DIR")
					if gitlabProjectDir != "" {
						gitlabProjectDir = gitlabProjectDir + "/"
						file = strings.ReplaceAll(file, gitlabProjectDir, "")
					}

					vulnerability := Vulnerability{
						Id:          strconv.Itoa(rand.Intn(999999-100000+1) + 100000),
						Name:        rule.Name,
						Message:     fmt.Sprintf("The pattern in rule: %s was found in a file", rule.Name),
						Description: fmt.Sprintf("The pattern in rule: %s was found in a file. Examine the file, remove and rotate your secret.", rule.Name),
						Location:    file,
						Line:        scannerPos,
					}
					log.Info(fmt.Sprintf("Possible Vulnerability Detected: %+v", vulnerability))
					scanner_results = append(scanner_results, vulnerability)
				}
			}
		}
		err = scanner.Err()
		if err != nil {
			// If an error occurred scanning, break out and go to next file
			log.Error(err)
			break
		}
	}

	log.Info("The scanner completed successfully")
	return scanner_results
}
