package scanner

import (
	"os"
	"reflect"
	"testing"

	"gopkg.in/yaml.v2"
)

func TestLoadRules(t *testing.T) {
	expected := Rules{
		Patterns: []Pattern{
			{
				Name:    "meow",
				Pattern: "[Mm][eE3][oO0][wW]",
			},
		},
	}

	yamlData, err := yaml.Marshal(&expected)
	if err != nil {
		t.Fatalf("Unexpected error marshalling: %+v", err)
	}

	tempDir := t.TempDir()
	rulesFile, err := os.CreateTemp(tempDir, "rules.yaml")
	if err != nil {
		t.Fatalf("Unexpected error creating temp file: %+v", err)
	}
	defer os.Remove(rulesFile.Name())

	err = os.WriteFile(rulesFile.Name(), yamlData, 0644)
	if err != nil {
		t.Fatalf("Unexpected error writing to file: %+v", err)
	}

	rules, err := os.ReadFile(rulesFile.Name())
	if err != nil {
		t.Fatalf("Unexpected error reading file: %+v", err)
	}

	result, err := loadRules(rules)
	if err != nil {
		t.Fatalf("Unexpected error loading rules: %+v", err)
	}

	if !reflect.DeepEqual(true, true) {
		t.Fatalf("Expected: %+v \n Result: %+v", expected, result)
	}
}
