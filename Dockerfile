FROM golang:latest

RUN apt-get install git
RUN apt-get update -y
RUN apt-get upgrade -y

WORKDIR /app
COPY . /app

RUN go mod download
RUN go build -o /app/fern-pattern-scanner

ENTRYPOINT []
CMD [ "/app/fern-pattern-scanner", "scan", "--report"]